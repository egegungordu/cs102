package cs102.week03;

public class DriverLicenseTest {
    public static void main(String args[]) {
        /* Construct an object */
        DriverLicense firstDriverLicense = new DriverLicense("Michael", "670LF", "2021", "A");
        DriverLicense secondDriverLicense = new DriverLicense("George", "768UT", "2025", "B");
        System.out.println(firstDriverLicense);
        System.out.println(secondDriverLicense);

        /* Increase Penalty */
        firstDriverLicense.increasePenalty(80);
        System.out.println(firstDriverLicense);

        /* Decrease Penalty  - Test "If->True" Path */
        firstDriverLicense.decreasePenalty(50);
        System.out.println(firstDriverLicense);

        /* Decrease Penalty  - Test "If->False" Path */
        firstDriverLicense.decreasePenalty(35);
        System.out.println(firstDriverLicense);

        /* Set Status - penalty > 100 case */
        firstDriverLicense.increasePenalty(135);
        System.out.println(firstDriverLicense);

        /* Set Status - status - false, penalty >= 20 case */
        firstDriverLicense.decreasePenalty(85);
        System.out.println(firstDriverLicense);

        /* Set Status - status - false, penalty < 20 case */
        firstDriverLicense.decreasePenalty(35);
        System.out.println(firstDriverLicense);
    }
}
